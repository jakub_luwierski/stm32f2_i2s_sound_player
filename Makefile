export

DEVICE_FAMILY = STM32F2xx
DEVICE_TYPE = STM32F205xx
STARTUP_FILE = stm32f205xx
SYSTEM_FILE = stm32f2xx

CMSIS = Drivers/CMSIS
CMSIS_DEVSUP = $(CMSIS)/Device/ST/$(DEVICE_FAMILY)/
CMSIS_OPT = -D$(DEVICE_TYPE) -DUSE_HAL_DRIVER
OTHER_OPT = -D__FPU_PRESENT=1 -D__SOFTFP__
CPU = -mthumb -mcpu=cortex-m3
SYSTEM = arm-none-eabi

LDSCRIPT = "Projects/TrueSTUDIO/stm32f2_i2s_sound_player Configuration/STM32F205RG_FLASH.ld"

SRCDIR := Src/
INCDIR := Inc/

LIBDIR := Drivers/
MDLDIR := Middlewares/

LIBINC := -IInc
LIBINC += -IDrivers/CMSIS/Device/ST/STM32F2xx/Include
LIBINC += -IDrivers/CMSIS/Include
LIBINC += -IDrivers/STM32F2xx_HAL_Driver/Inc
LIBINC += -IMiddlewares/ST/STM32_USB_Device_Library/Core/Inc
LIBINC += -IMiddlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc
LIBINC += -IMiddlewares/ST/STM32_USB_Host_Library/Core/Inc
LIBINC += -IMiddlewares/ST/STM32_USB_Host_Library/Class/MSC/Inc
LIBINC += -IMiddlewares/Third_Party/FatFs/src

LIBS := ./$(LIBDIR)/STM32F2xx_HAL_Driver/libstm32fw.a
LIBS += ./$(MDLDIR)/Third_Party/FatFs/fatfs.a
LIBS += ./$(MDLDIR)/ST/STM32_USB_Host_Library/libstm32usbhost.a
LIBS += ./$(MDLDIR)/ST/STM32_USB_Device_Library/libstm32usbdev.a

LIBS += -lm -lgcc -lc
CC      = $(SYSTEM)-gcc
CCDEP   = $(SYSTEM)-gcc
LD      = $(SYSTEM)-gcc
AR      = $(SYSTEM)-ar
AS      = $(SYSTEM)-gcc
OBJCOPY = $(SYSTEM)-objcopy
OBJDUMP	= $(SYSTEM)-objdump
GDB		= $(SYSTEM)-gdb
SIZE	= $(SYSTEM)-size
OCD	= sudo ~/openocd-git/openocd/src/openocd \
		-s ~/openocd-git/openocd/tcl/ \
		-f interface/stlink-v2.cfg \
                -f target/stm32f2x_stlink.cfg

INCLUDES = $(LIBINC)
CFLAGS  = $(CPU) $(CMSIS_OPT) $(OTHER_OPT) -Wall -Wno-return-type -Wno-missing-braces -Wno-unused-but-set-variable -Wno-unused-function -fno-common -fno-strict-aliasing -Wno-pointer-sign -Os $(INCLUDES) -g -Wfatal-errors
ASFLAGS = $(CFLAGS) -x assembler-with-cpp
LDFLAGS = --specs=nosys.specs -T $(LDSCRIPT) $(CPU)
ARFLAGS = cr
OBJCOPYFLAGS = -Obinary
OBJDUMPFLAGS = -S

STARTUP_OBJ = $(CMSIS_DEVSUP)/Source/Templates/gcc/startup_$(STARTUP_FILE).o
SYSTEM_OBJ = $(CMSIS_DEVSUP)/Source/Templates/system_$(SYSTEM_FILE).o

BIN = main.bin

OBJS = $(sort \
 $(patsubst %.c,%.o,$(wildcard Src/*.c)) \
 $(patsubst %.s,%.o,$(wildcard Src/*.s)) \
 $(STARTUP_OBJ) \
 $(SYSTEM_OBJ))

all: $(BIN)

reset:
	$(OCD) -c init -c "reset run" -c shutdown
	#$(GDB) main.out <reset.gdb

flash: $(BIN)
	$(OCD) -c init -c "reset halt" \
	               -c "flash write_image erase "$(BIN)" 0x08000000" \
			       -c "reset run" \
	               -c shutdown
	
$(BIN): main.out
	$(OBJCOPY) $(OBJCOPYFLAGS) main.out $(BIN)
	$(OBJDUMP) $(OBJDUMPFLAGS) main.out > main.list
	$(SIZE) main.out
	@echo Make finished

main.out: $(LIBS) $(OBJS)
	$(LD) $(LDFLAGS) -o $@ $(OBJS) $(LIBS)

$(LIBS): libs

libs:
	@$(MAKE) -C $(LIBDIR)
	@$(MAKE) -C $(MDLDIR)

libclean: clean
	@$(MAKE) -C $(LIBDIR) clean
	@$(MAKE) -C $(MDLDIR) clean

clean:
	-rm -f $(OBJS)
	-rm -f main.list main.out main.hex main.map main.bin .depend

depend dep: .depend

include .depend

.depend: Src/*.c
	$(CCDEP) $(CFLAGS) -MM $^ | sed -e 's@.*.o:@Src/&@' > .depend 

.c.o:
	@echo cc $<
	@$(CC) $(CFLAGS) -c -o $@ $<

.s.o:
	@echo as $<
	@$(AS) $(ASFLAGS) -c -o $@ $<
# DO NOT DELETE
