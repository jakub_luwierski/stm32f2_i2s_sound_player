#!/bin/bash
find * -type f -name *.c -exec dos2unix {} \;
find * -type f -name *.h -exec dos2unix {} \;
find * -type f -name *.s -exec dos2unix {} \;